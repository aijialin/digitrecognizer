import tensorflow as tf
#clear warning
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

#import digitData
#digit_data = digitData.get_data("train.csv")

from mnist import train, validation, test

# 这个tensor的形状是[None, 784], 表示第一维可以是任何长度的
x = tf.placeholder(tf.float32, [None, 784])

# 初始化权重和偏置量
W = tf.Variable(tf.zeros([784, 10]))
b = tf.Variable(tf.zeros([10]))

# softmax 回归（ softmax regression ）分两步：首先对输入被分类对象属于某个类的“证据”相加求和，然后将这个“证据”的和转化为概率 
y = tf.nn.softmax(tf.matmul(x, W) + b)

# 为了计算交叉熵，我们首先需要添加一个新的占位符用于输入正确值：
y_ = tf.placeholder(tf.float32, [None, 10])
cross_entropy = -tf.reduce_sum(y_*tf.log(y))

# 在这里，我们要求 TensorFlow 用梯度下降算法
train_step = tf.train.GradientDescentOptimizer(0.01).minimize(cross_entropy)

init = tf.global_variables_initializer()

# 现在我们可以在一个 Session 里面启动我们的模型，并且初始化变量：
sess = tf.Session()
sess.run(init)

# 然后开始训练模型，这里我们让模型循环训练 1000 次！
for i in range(1000):
	batch_xs, batch_ys = train.next_batch(100)
	sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})

#sess.run(train_step, feed_dict={x: train.images, y_: train.labels})

# 评估我们的模型
correct_prediction = tf.equal(tf.argmax(y,1), tf.argmax(y_,1))

accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

print(sess.run(accuracy, feed_dict={x: validation.images, y_: validation.labels}))






